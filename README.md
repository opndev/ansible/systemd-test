# README.md

This is a role to show case systemd usage with ansible and docker.

In `molecule/default/molecule.yml` you'll find the following snippet:

```

platforms:
  - name: debian-testing
    #image: registry.gitlab.com/opndev/ansible/docker:latest
    image: geerlingguy/docker-debian12-ansible
    privileged: true
    pre_build_image: true
    cgroupns_mode: host
    volume_mounts:
      - /sys/fs/cgroup:/sys/fs/cgroup:rw
      - /run
      - /run/lock
```

When running `molecule converge` you will get the following error:

```
TASK [opndev.systemdtest : Start and enable openntpd] **************************
fatal: [debian-testing]: FAILED! => {"changed": false, "msg": "Service is in unknown state", "status": {}}
```

This is because systemd is not running, instead some other script is running as
a poor man's daemon:

```
$ docker exec debian-testing ps -ef
UID          PID    PPID  C STIME TTY          TIME CMD
root           1       0  0 19:28 ?        00:00:00 bash -c while true; do sleep 10000; done
root           7       1  0 19:28 ?        00:00:00 sleep 10000
root        1151       0  0 19:29 ?        00:00:00 ps -ef
```

The docker image however has a command defined:

```
docker history geerlingguy/docker-debian12-ansible
IMAGE          CREATED       CREATED BY                                      SIZE      COMMENT
35602656da36   2 days ago    CMD ["/lib/systemd/systemd"]                    0B        buildkit.dockerfile.v0
<missing>      2 days ago    VOLUME [/sys/fs/cgroup]                         0B        buildkit.dockerfile.v0
<missing>      2 days ago    RUN |1 DEBIAN_FRONTEND=noninteractive /bin/s…   0B        buildkit.dockerfile.v0

[snip]

<missing>      2 days ago    LABEL maintainer=Jeff Geerling                  0B        buildkit.dockerfile.v0
<missing>      3 weeks ago   /bin/sh -c #(nop)  CMD ["bash"]                 0B
```


Now run `molecule destroy` and start the container manually and execute the
prepare steps manually:

```
docker run --rm -d --name debian-testing --privileged --cgroupns host -v /sys/fs/cgroup:/sys/fs/cgroup:rw -v /run -v /run/lock geerlingguy/docker-debian12-ansible

docker exec -ti debian-testing bash

# You are now in the container:
apt-get update
apt-get install lsb-release
# exit the container
```

Now run `molecule converge` again and notice that the failing step succeeds:

```
TASK [opndev.systemdtest : Start and enable openntpd] **************************
changed: [debian-testing]
```

This is because systemd now is running:
```
$ docker exec -ti debian-testing ps -ef
UID          PID    PPID  C STIME TTY          TIME CMD
root           1       0  0 19:54 ?        00:00:00 /lib/systemd/systemd
root          20       1  0 19:54 ?        00:00:00 /lib/systemd/systemd-journald
root          30       0 16 19:54 pts/1    00:00:00 ps -ef
```

Run `molecule destroy` and change `molecule/default/molecule.yml` to have a
command:

```
platforms:
  - name: debian-testing
    #image: registry.gitlab.com/opndev/ansible/docker:latest
    image: geerlingguy/docker-debian12-ansible
    privileged: true
    pre_build_image: true
    cgroupns_mode: host
    volume_mounts:
      - /sys/fs/cgroup:/sys/fs/cgroup:rw
      - /run
      - /run/lock
    command: /lib/systemd/systemd
```

Notice that you now get an error when running converge:

```
fatal: [debian-testing]: UNREACHABLE! => {"changed": false, "msg": "Failed to create temporary directory. In some cases, you may have been able to authenticate and did not have permissions on the target directory. Consider changing the remote tmp path in ansible.cfg to a path rooted in \"/tmp\", for more error information use -vvv. Failed command was: ( umask 77 && mkdir -p \"` echo ~/.ansible/tmp `\"&& mkdir \"` echo ~/.ansible/tmp/ansible-tmp-1704829251.8463857-1328986-266520883513731 `\" && echo ansible-tmp-1704829251.8463857-1328986-266520883513731=\"` echo ~/.ansible/tmp/ansible-tmp-1704829251.8463857-1328986-266520883513731 `\" ), exited with result 1", "unreachable": true}
```

It doesn't matter what the command is, it will fail, you can leave it empty:
`command:` or set an empty string `command: ""`, it will fail with the same
error. This seems to be related to [the following bug](https://github.com/ansible/molecule/issues/3818)

## Solution

libera user cavcrosby mentioned using `volumes` over `volume_mounts`, and with
setting `command` to an empty string. While reluctant because of the empty
command bug it did work.
